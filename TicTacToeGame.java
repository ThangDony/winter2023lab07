

import java.util.Scanner;

public class TicTacToeGame {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        
        System.out.println("---Welcome to Tic Tac Toe!---");
        System.out.println("Player 1's token: X");
        System.out.println("Player 2's token: O");
        
        boolean newGame = true;

        // Game loop
        while (newGame) {
            Board board = new Board();
            boolean gameOver = false;
            int player = 1;
            Square playerToken = Square.X;

            while (!gameOver) {
                // Print board
                System.out.println(board);
    
                // Set player token based on player turn
                if (player == 1) {
                    playerToken = Square.X;
                } else {
                    playerToken = Square.O;
                }
    
                int row, col;
                do {
                    System.out.println("Player " + player + ": Enter row and column (1-3) to place your token:");
                    row = keyboard.nextInt() - 1;
                    col = keyboard.nextInt() - 1;
                } while (!board.placeToken(row, col, playerToken));
    
                // Check for winner or tie
                if (board.checkIfFull() && !(board.checkIfWinning(playerToken))) {
                    System.out.println(board);
                    System.out.println("It's a tie!");
                    gameOver = true;
                } else if (board.checkIfWinning(playerToken)) {
                    System.out.println(board);
                    System.out.println("** Player " + player + " wins! **");
                    gameOver = true;
                } else {
                // Switch player turn
                    player++;
                    if (player > 2) {
                        player = 1;
                    }
                }
            }

            System.out.println("Play again? (Y/N)");
            String playAgain = keyboard.next();
            if (!playAgain.toUpperCase().equals("Y")) {
                newGame = false;
            }

        }
        keyboard.close();
    }
}

