public class Board {

    private Square[][] tictactoeBoard;

    public Board() {
        tictactoeBoard = new Square[3][3];

        for (int i=0; i<tictactoeBoard.length; i++) {
            for (int j=0; j<tictactoeBoard[i].length; j++) {
                tictactoeBoard[i][j] = Square.BLANK;
            }
        }
    }
    
    public String toString() {
        String result = "";
        for (int i = 0; i < tictactoeBoard.length; i++) {
            for (int j = 0; j < tictactoeBoard[i].length; j++) {
                result += tictactoeBoard[i][j] + " ";
            }
            result += "\n";
        }
        return result;
    }

    public boolean placeToken(int row, int col, Square playerToken) {
        if (row < 0 || row >= tictactoeBoard.length || col < 0 || col >= tictactoeBoard[row].length) {
            return false;
        }
        if (tictactoeBoard[row][col] == Square.BLANK) {
            tictactoeBoard[row][col] = playerToken;
            return true;
        }
        return false;
    }

    public boolean checkIfFull() {
        for (int i = 0; i < tictactoeBoard.length; i++) {
            for (int j = 0; j < tictactoeBoard[i].length; j++) {
                if (tictactoeBoard[i][j] == Square.BLANK) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean checkIfWinningHorizontal(Square playerToken) {
        for (int i = 0; i < tictactoeBoard.length; i++) {
            boolean win = true;
            for (int j = 0; j < tictactoeBoard[i].length; j++) {
                if (tictactoeBoard[i][j] != playerToken) {
                    win = false;
                    break;
                }
            } if (win) {
                return true;
            }
        }
        return false;
    }

    private boolean checkIfWinningVertical(Square playerToken) {
        for (int j = 0; j < tictactoeBoard[0].length; j++) {
            boolean win = true;
            for (int i = 0; i < tictactoeBoard.length; i++) {
                if (tictactoeBoard[i][j] != playerToken) {
                    win = false;
                    break;
                }
            } if (win) {
                return true;
            }
        }
        return false;
    }

    private boolean checkIfWinningDiagonal(Square playerToken) {
        if (playerToken == tictactoeBoard[0][0] && playerToken == tictactoeBoard[1][1] && playerToken == tictactoeBoard[2][2]) {
            return true;
        }
        if (playerToken == tictactoeBoard[2][0] && playerToken == tictactoeBoard[1][1] && playerToken == tictactoeBoard[0][2]) {
            return true;
        }
        return false;
    }

    public boolean checkIfWinning(Square playerToken) {
        if (checkIfWinningVertical(playerToken) || checkIfWinningHorizontal(playerToken) || checkIfWinningDiagonal(playerToken)) {
            return true;
        } else {
            return false;
        }
    }

}
